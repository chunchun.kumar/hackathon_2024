package com.kaleidofin.app.web.rest.dto;

public class DADto {
    private String poolName;
    private String kiRating;
    private String state;
    private String applicantName;
    private String totalOS;
    private String bcInterestRate;
    private String tenure;
    private String maturingYear;
    private String dpd;
    private String product;
    private String paymentFrequency;
    private String balTenor;
    private String loanCycle;
    private String secured;
    private String purposeOfLoan;
    private String subSector;
    private String typeOfProperty;
    private String cibilScore;
    private String mortagage;
    private String seaoning;
    private String gender;
    private String poolAvailability;

    public String getPoolName() {
        return poolName;
    }

    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    public String getKiRating() {
        return kiRating;
    }

    public void setKiRating(String kiRating) {
        this.kiRating = kiRating;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public String getTotalOS() {
        return totalOS;
    }

    public void setTotalOS(String totalOS) {
        this.totalOS = totalOS;
    }

    public String getBcInterestRate() {
        return bcInterestRate;
    }

    public void setBcInterestRate(String bcInterestRate) {
        this.bcInterestRate = bcInterestRate;
    }

    public String getTenure() {
        return tenure;
    }

    public void setTenure(String tenure) {
        this.tenure = tenure;
    }

    public String getMaturingYear() {
        return maturingYear;
    }

    public void setMaturingYear(String maturingYear) {
        this.maturingYear = maturingYear;
    }

    public String getDpd() {
        return dpd;
    }

    public void setDpd(String dpd) {
        this.dpd = dpd;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getPaymentFrequency() {
        return paymentFrequency;
    }

    public void setPaymentFrequency(String paymentFrequency) {
        this.paymentFrequency = paymentFrequency;
    }

    public String getBalTenor() {
        return balTenor;
    }

    public void setBalTenor(String balTenor) {
        this.balTenor = balTenor;
    }

    public String getLoanCycle() {
        return loanCycle;
    }

    public void setLoanCycle(String loanCycle) {
        this.loanCycle = loanCycle;
    }

    public String getSecured() {
        return secured;
    }

    public void setSecured(String secured) {
        this.secured = secured;
    }

    public String getPurposeOfLoan() {
        return purposeOfLoan;
    }

    public void setPurposeOfLoan(String purposeOfLoan) {
        this.purposeOfLoan = purposeOfLoan;
    }

    public String getSubSector() {
        return subSector;
    }

    public void setSubSector(String subSector) {
        this.subSector = subSector;
    }

    public String getTypeOfProperty() {
        return typeOfProperty;
    }

    public void setTypeOfProperty(String typeOfProperty) {
        this.typeOfProperty = typeOfProperty;
    }

    public String getCibilScore() {
        return cibilScore;
    }

    public void setCibilScore(String cibilScore) {
        this.cibilScore = cibilScore;
    }

    public String getMortagage() {
        return mortagage;
    }

    public void setMortagage(String mortagage) {
        this.mortagage = mortagage;
    }

    public String getSeaoning() {
        return seaoning;
    }

    public void setSeaoning(String seaoning) {
        this.seaoning = seaoning;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPoolAvailability() {
        return poolAvailability;
    }

    public void setPoolAvailability(String poolAvailability) {
        this.poolAvailability = poolAvailability;
    }
}
