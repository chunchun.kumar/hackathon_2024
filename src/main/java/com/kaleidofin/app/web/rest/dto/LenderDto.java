package com.kaleidofin.app.web.rest.dto;

public class LenderDto {

    private String bcName;
    private String location;
    private String loanAmount;
    private String assetClass;
    private String interestRate;
    private String loanPSLClassification;
    private String gender;
    private String caste;
    private String subSectorCode;
    private String cityType;
    private String lenderName;
    private String yearsOfOperation;
    private String applicationDate;
    private String disbursementDate;
    private String dpdAsOnDate;
    private String fLDGOffered;

    public String getBcName() {
        return bcName;
    }

    public void setBcName(String bcName) {
        this.bcName = bcName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getAssetClass() {
        return assetClass;
    }

    public void setAssetClass(String assetClass) {
        this.assetClass = assetClass;
    }

    public String getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }

    public String getLoanPSLClassification() {
        return loanPSLClassification;
    }

    public void setLoanPSLClassification(String loanPSLClassification) {
        this.loanPSLClassification = loanPSLClassification;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCaste() {
        return caste;
    }

    public void setCaste(String caste) {
        this.caste = caste;
    }

    public String getSubSectorCode() {
        return subSectorCode;
    }

    public void setSubSectorCode(String subSectorCode) {
        this.subSectorCode = subSectorCode;
    }

    public String getCityType() {
        return cityType;
    }

    public void setCityType(String cityType) {
        this.cityType = cityType;
    }

    public String getLenderName() {
        return lenderName;
    }

    public void setLenderName(String lenderName) {
        this.lenderName = lenderName;
    }

    public String getYearsOfOperation() {
        return yearsOfOperation;
    }

    public void setYearsOfOperation(String yearsOfOperation) {
        this.yearsOfOperation = yearsOfOperation;
    }

    public String getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getDisbursementDate() {
        return disbursementDate;
    }

    public void setDisbursementDate(String disbursementDate) {
        this.disbursementDate = disbursementDate;
    }

    public String getDpdAsOnDate() {
        return dpdAsOnDate;
    }

    public void setDpdAsOnDate(String dpdAsOnDate) {
        this.dpdAsOnDate = dpdAsOnDate;
    }

    public String getfLDGOffered() {
        return fLDGOffered;
    }

    public void setfLDGOffered(String fLDGOffered) {
        this.fLDGOffered = fLDGOffered;
    }
}
