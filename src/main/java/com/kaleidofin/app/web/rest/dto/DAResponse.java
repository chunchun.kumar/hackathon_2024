package com.kaleidofin.app.web.rest.dto;

public class DAResponse {


    private String poolName;
    private Long volume;
    private Double averageSeasoning;
    private int searchMatchPercentage;
    private String poolAvailability;
    private Double kiRating;

    public String getPoolName() {
        return poolName;
    }

    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    public Long getVolume() {
        return volume;
    }

    public void setVolume(Long volume) {
        this.volume = volume;
    }

    public Double getAverageSeasoning() {
        return averageSeasoning;
    }

    public void setAverageSeasoning(Double averageSeasoning) {
        this.averageSeasoning = averageSeasoning;
    }

    public int getSearchMatchPercentage() {
        return searchMatchPercentage;
    }

    public void setSearchMatchPercentage(int searchMatchPercentage) {
        this.searchMatchPercentage = searchMatchPercentage;
    }

    public String getPoolAvailability() {
        return poolAvailability;
    }

    public void setPoolAvailability(String poolAvailability) {
        this.poolAvailability = poolAvailability;
    }

    public Double getKiRating() {
        return kiRating;
    }

    public void setKiRating(Double kiRating) {
        this.kiRating = kiRating;
    }
}
