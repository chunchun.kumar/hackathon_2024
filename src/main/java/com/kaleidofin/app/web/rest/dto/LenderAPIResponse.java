package com.kaleidofin.app.web.rest.dto;

public class LenderAPIResponse {
    private String bcName;
    private Double averageTicketSize;
    private String areaOfOperation;
    private int searchMatchPercentage;
    private Double averageTAT;
    private String currentStatus;
    private Double kiRating;

    public String getBcName() {
        return bcName;
    }

    public void setBcName(String bcName) {
        this.bcName = bcName;
    }

    public Double getAverageTicketSize() {
        return averageTicketSize;
    }

    public void setAverageTicketSize(Double averageTicketSize) {
        this.averageTicketSize = averageTicketSize;
    }

    public String getAreaOfOperation() {
        return areaOfOperation;
    }

    public void setAreaOfOperation(String areaOfOperation) {
        this.areaOfOperation = areaOfOperation;
    }

    public int getSearchMatchPercentage() {
        return searchMatchPercentage;
    }

    public void setSearchMatchPercentage(int searchMatchPercentage) {
        this.searchMatchPercentage = searchMatchPercentage;
    }

    public Double getAverageTAT() {
        return averageTAT;
    }

    public void setAverageTAT(Double averageTAT) {
        this.averageTAT = averageTAT;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public Double getKiRating() {
        return kiRating;
    }

    public void setKiRating(Double kiRating) {
        this.kiRating = kiRating;
    }
}
