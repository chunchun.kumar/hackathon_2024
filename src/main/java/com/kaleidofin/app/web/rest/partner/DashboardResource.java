package com.kaleidofin.app.web.rest.partner;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kaleidofin.app.web.rest.dto.DADto;
import com.kaleidofin.app.web.rest.dto.DAResponse;
import com.kaleidofin.app.web.rest.dto.LenderAPIResponse;
import com.kaleidofin.app.web.rest.dto.LenderDto;
import io.micrometer.core.instrument.util.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class DashboardResource {

    @GetMapping("/lenders")
    public ResponseEntity<List<LenderAPIResponse>> getLenderData(@RequestParam(required = false) String location,
                                                                 @RequestParam(required = false) String loanAmount,
                                                                 @RequestParam(required = false) String loanPSLClassification,
                                                                 @RequestParam(required = false) String gender,
                                                                 @RequestParam(required = false) String subSectorCode,
                                                                 @RequestParam(required = false) String cityType,
                                                                 @RequestParam(required = false) String sortBy
    ) throws IOException {
        final List<LenderAPIResponse>[] lenderAPIResponse = new List[]{new ArrayList<>()};
        List<LenderDto> lenderDtoList = lenderDtos();
        List<String> orderList = lenderDtoList.stream().map(LenderDto::getBcName).distinct().collect(Collectors.toList());
        List<LenderDto> finalLenderDtoList = lenderDtoList;
        List<LenderDto> finalLenderDtoList1 = lenderDtoList;
        orderList.forEach(s -> {

            LenderAPIResponse lenderAPIResponse1 = new LenderAPIResponse();
            lenderAPIResponse1.setBcName(s);
            List<LenderDto> lenderDtos = finalLenderDtoList1.stream().filter(lenderDto -> StringUtils.equalsIgnoreCase(lenderDto.getBcName(), s)).collect(Collectors.toList());
            List<LenderDto> lenderDtos1 = lenderDtos;
            if (StringUtils.isNotBlank(location)) {
                lenderDtos1 = lenderDtos1.stream().filter(lenderDto -> StringUtils.equalsIgnoreCase(lenderDto.getLocation(), location)).collect(Collectors.toList());
            }
            if (StringUtils.isNotBlank(loanAmount)) {
                if("lessThanOrEqualTo1Lakh".equalsIgnoreCase(loanAmount)) {
                    lenderDtos1 = lenderDtos1.stream().filter(lenderDto -> {
                        Double la = Double.valueOf(lenderDto.getLoanAmount());
                        return la<=100000D;
                    }).collect(Collectors.toList());
                } else if ("greaterThan1LakhAndlessThanOrEqualTo5Lakh".equalsIgnoreCase(loanAmount)) {
                    lenderDtos1 = lenderDtos1.stream().filter(lenderDto -> {
                        Double la = Double.valueOf(lenderDto.getLoanAmount());
                        return la>100000D && la<=500000D;
                    }).collect(Collectors.toList());
                } else if ("greaterThan5Lakh".equalsIgnoreCase(loanAmount)) {
                    lenderDtos1 = lenderDtos1.stream().filter(lenderDto -> {
                        Double la = Double.valueOf(lenderDto.getLoanAmount());
                        return la>=500000D;
                    }).collect(Collectors.toList());
                }
            }
            if (StringUtils.isNotBlank(loanPSLClassification)) {
                lenderDtos1 = lenderDtos1.stream().filter(lenderDto -> StringUtils.equalsIgnoreCase(lenderDto.getLoanPSLClassification(), loanPSLClassification)).collect(Collectors.toList());
            }
            if (StringUtils.isNotBlank(gender)) {
                lenderDtos1 = lenderDtos1.stream().filter(lenderDto -> StringUtils.equalsIgnoreCase(lenderDto.getGender(), gender)).collect(Collectors.toList());
            }
            if (StringUtils.isNotBlank(subSectorCode)) {
                lenderDtos1 = lenderDtos1.stream().filter(lenderDto -> StringUtils.equalsIgnoreCase(lenderDto.getSubSectorCode(), subSectorCode)).collect(Collectors.toList());
            }
            if (StringUtils.isNotBlank(cityType)) {
                lenderDtos1 = lenderDtos1.stream().filter(lenderDto -> StringUtils.equalsIgnoreCase(lenderDto.getCityType(), cityType)).collect(Collectors.toList());
            }
            Double averageTicketSize = lenderDtos1.stream().mapToDouble(i -> Double.parseDouble(i.getLoanAmount())).sum() / lenderDtos.size();
            lenderAPIResponse1.setAverageTicketSize(averageTicketSize);
            String areaOfOperation = lenderDtos1.stream().map(LenderDto::getLocation).distinct().collect(Collectors.joining(", "));
            lenderAPIResponse1.setAreaOfOperation(areaOfOperation);
            lenderAPIResponse1.setSearchMatchPercentage((int) (((double) lenderDtos1.size() / lenderDtos.size()) * 100));

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            lenderAPIResponse1.setAverageTAT(lenderDtos1.stream().mapToDouble(value -> ChronoUnit.DAYS
                    .between(
                            LocalDate.parse(value.getApplicationDate(), formatter), LocalDate.parse(value.getDisbursementDate(), formatter))).sum() / lenderDtos.size());
            if (lenderDtos1.size() >= 1) {
                if (StringUtils.equalsIgnoreCase(lenderDtos1.get(0).getBcName(), "Magalir")) {
                    lenderAPIResponse1.setCurrentStatus("Active");
                    lenderAPIResponse1.setKiRating(3.2D);
                } else if (StringUtils.equalsIgnoreCase(lenderDtos1.get(0).getBcName(), "MSM")) {
                    lenderAPIResponse1.setCurrentStatus("Onboarded");
                    lenderAPIResponse1.setKiRating(3.7D);
                } else if (StringUtils.equalsIgnoreCase(lenderDtos1.get(0).getBcName(), "Swasti")) {
                    lenderAPIResponse1.setCurrentStatus("Onboarding");
                    lenderAPIResponse1.setKiRating(4.0D);
                } else {
                    lenderAPIResponse1.setCurrentStatus("Shortlisted");
                    lenderAPIResponse1.setKiRating(4.5D);
                }
                if (StringUtils.equalsIgnoreCase(sortBy, "rating")) {
                    lenderAPIResponse[0] = lenderAPIResponse[0].stream().sorted(Comparator.comparingDouble(p -> -p.getKiRating())).collect(Collectors.toList());
                }
                if (StringUtils.equalsIgnoreCase(sortBy, "matchPercentage")) {
                    lenderAPIResponse[0] = lenderAPIResponse[0].stream().sorted(Comparator.comparingDouble(p -> -p.getSearchMatchPercentage())).collect(Collectors.toList());
                }
                lenderAPIResponse[0].add(lenderAPIResponse1);
            }

        });
        return ResponseEntity.ok().body(lenderAPIResponse[0]);
    }

    private List<LenderDto> lenderDtos() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String ins = "[{\"bcName\":\"Magalir\",\"location\":\"Gujarat\",\"loanAmount\":50000,\"assetClass\":\"WEL\",\"interestRate\":17,\"loanPSLClassification\":\"Small Artisians\",\"gender\":\"Female\",\"caste\":\"OBC\",\"subSectorCode\":\"Manufacture of Textiles\",\"applicationDate\":\"2018-02-14\",\"disbursementDate\":\"2018-02-23\",\"dpdAsOnDate\":54,\"cityType\":\"B30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":10},{\"bcName\":\"Magalir\",\"location\":\"Gujarat\",\"loanAmount\":60000,\"assetClass\":\"WEL\",\"interestRate\":22,\"loanPSLClassification\":\"Small and Marginal farmer\",\"gender\":\"Female\",\"caste\":\"OBC\",\"subSectorCode\":\"Farming of Animals, Poultry\",\"applicationDate\":\"2017-05-03\",\"disbursementDate\":\"2017-05-11\",\"dpdAsOnDate\":18,\"cityType\":\"T30\",\"lenderName\":\"A\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":10},{\"bcName\":\"Magalir\",\"location\":\"Gujarat\",\"loanAmount\":50000,\"assetClass\":\"IL\",\"interestRate\":18,\"loanPSLClassification\":\"NRLM Benificiary\",\"gender\":\"Female\",\"caste\":\"OBC\",\"subSectorCode\":\"Manufacture of Textiles\",\"applicationDate\":\"2018-10-22\",\"disbursementDate\":\"2018-10-30\",\"dpdAsOnDate\":44,\"cityType\":\"T30\",\"lenderName\":\"B\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":10},{\"bcName\":\"Magalir\",\"location\":\"Gujarat\",\"loanAmount\":70000,\"assetClass\":\"IL\",\"interestRate\":25,\"loanPSLClassification\":\"Small and Marginal farmer\",\"gender\":\"Female\",\"caste\":\"ST\",\"subSectorCode\":\"Farming of Animals, Poultry\",\"applicationDate\":\"2017-07-09\",\"disbursementDate\":\"2017-07-18\",\"dpdAsOnDate\":0,\"cityType\":\"T30\",\"lenderName\":\"B\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":10},{\"bcName\":\"Magalir\",\"location\":\"Gujarat\",\"loanAmount\":50000,\"assetClass\":\"WEL\",\"interestRate\":14,\"loanPSLClassification\":\"SC and ST tribe\",\"gender\":\"Other\",\"caste\":\"ST\",\"subSectorCode\":\"Manufacture of furniture\",\"applicationDate\":\"2018-11-30\",\"disbursementDate\":\"2018-12-06\",\"dpdAsOnDate\":0,\"cityType\":\"B30\",\"lenderName\":\"A\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":10},{\"bcName\":\"Magalir\",\"location\":\"Uttar Pradesh\",\"loanAmount\":60000,\"assetClass\":\"WEL\",\"interestRate\":21,\"loanPSLClassification\":\"SC and ST tribe\",\"gender\":\"Female\",\"caste\":\"ST\",\"subSectorCode\":\"Manufacture of Textiles\",\"applicationDate\":\"2017-04-17\",\"disbursementDate\":\"2017-04-26\",\"dpdAsOnDate\":0,\"cityType\":\"T30\",\"lenderName\":\"B\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":10},{\"bcName\":\"Magalir\",\"location\":\"Uttar Pradesh\",\"loanAmount\":500000,\"assetClass\":\"Tractor Loan\",\"interestRate\":26,\"loanPSLClassification\":\"Distressed farmer\",\"gender\":\"Male\",\"caste\":\"OBC\",\"subSectorCode\":\"Agriculture & Related Service Activities\",\"applicationDate\":\"2018-08-05\",\"disbursementDate\":\"2018-08-11\",\"dpdAsOnDate\":0,\"cityType\":\"B30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":10},{\"bcName\":\"Magalir\",\"location\":\"Madhya Pradesh\",\"loanAmount\":100000,\"assetClass\":\"Two-wheeler loan\",\"interestRate\":20,\"loanPSLClassification\":\"NRLM Benificiary\",\"gender\":\"Male\",\"caste\":\"OBC\",\"subSectorCode\":\"CONSTRUCTION\",\"applicationDate\":\"2017-01-12\",\"disbursementDate\":\"2017-01-21\",\"dpdAsOnDate\":0,\"cityType\":\"T30\",\"lenderName\":\"A\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":10},{\"bcName\":\"Magalir\",\"location\":\"Madhya Pradesh\",\"loanAmount\":600000,\"assetClass\":\"KCC loan\",\"interestRate\":26,\"loanPSLClassification\":\"SC and ST tribe\",\"gender\":\"Female\",\"caste\":\"ST\",\"subSectorCode\":\"Agriculture & Related Service Activities\",\"applicationDate\":\"2018-06-28\",\"disbursementDate\":\"2018-07-08\",\"dpdAsOnDate\":0,\"cityType\":\"B30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":10},{\"bcName\":\"Magalir\",\"location\":\"Madhya Pradesh\",\"loanAmount\":500000,\"assetClass\":\"WEL\",\"interestRate\":14,\"loanPSLClassification\":\"SC and ST tribe\",\"gender\":\"Female\",\"caste\":\"SC\",\"subSectorCode\":\"CONSTRUCTION\",\"applicationDate\":\"2017-09-19\",\"disbursementDate\":\"2017-09-25\",\"dpdAsOnDate\":0,\"cityType\":\"B30\",\"lenderName\":\"A\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":10},{\"bcName\":\"Magalir\",\"location\":\"Madhya Pradesh\",\"loanAmount\":800000,\"assetClass\":\"JLG\",\"interestRate\":24,\"loanPSLClassification\":\"Export credit\",\"gender\":\"Male\",\"caste\":\"General\",\"subSectorCode\":\"Manufacture of Beverages\",\"applicationDate\":\"2018-03-08\",\"disbursementDate\":\"2018-03-16\",\"dpdAsOnDate\":0,\"cityType\":\"B30\",\"lenderName\":\"A\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":10},{\"bcName\":\"MSM\",\"location\":\"Tamil Nadu\",\"loanAmount\":700000,\"assetClass\":\"JLG\",\"interestRate\":22,\"loanPSLClassification\":\"Minority community\",\"gender\":\"Male\",\"caste\":\"ST\",\"subSectorCode\":\"Farming of Animals, Poultry\",\"applicationDate\":\"2017-12-04\",\"disbursementDate\":\"2017-12-12\",\"dpdAsOnDate\":0,\"cityType\":\"T30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":15},{\"bcName\":\"MSM\",\"location\":\"Tamil Nadu\",\"loanAmount\":800000,\"assetClass\":\"KCC loan\",\"interestRate\":26,\"loanPSLClassification\":\"SC and ST tribe\",\"gender\":\"Male\",\"caste\":\"ST\",\"subSectorCode\":\"Agriculture & Related Service Activities\",\"applicationDate\":\"2018-10-11\",\"disbursementDate\":\"2018-10-16\",\"dpdAsOnDate\":0,\"cityType\":\"B30\",\"lenderName\":\"A\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":15},{\"bcName\":\"MSM\",\"location\":\"Tamil Nadu\",\"loanAmount\":800000,\"assetClass\":\"IL\",\"interestRate\":20,\"loanPSLClassification\":\"Person with disability\",\"gender\":\"Male\",\"caste\":\"General\",\"subSectorCode\":\"Manufacture of Textiles\",\"applicationDate\":\"2017-05-21\",\"disbursementDate\":\"2017-05-28\",\"dpdAsOnDate\":82,\"cityType\":\"T30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":15},{\"bcName\":\"MSM\",\"location\":\"Tamil Nadu\",\"loanAmount\":600000,\"assetClass\":\"KCC loan\",\"interestRate\":25,\"loanPSLClassification\":\"SC and ST tribe\",\"gender\":\"Female\",\"caste\":\"SC\",\"subSectorCode\":\"Agriculture & Related Service Activities\",\"applicationDate\":\"2018-02-26\",\"disbursementDate\":\"2018-03-05\",\"dpdAsOnDate\":28,\"cityType\":\"T30\",\"lenderName\":\"B\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":15},{\"bcName\":\"MSM\",\"location\":\"Gujarat\",\"loanAmount\":150000,\"assetClass\":\"Two-wheeler loan\",\"interestRate\":26,\"loanPSLClassification\":\"Small and Marginal farmer\",\"gender\":\"Male\",\"caste\":\"ST\",\"subSectorCode\":\"Farming of Animals, Poultry\",\"applicationDate\":\"2017-07-16\",\"disbursementDate\":\"2017-07-26\",\"dpdAsOnDate\":0,\"cityType\":\"B30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":15},{\"bcName\":\"MSM\",\"location\":\"Gujarat\",\"loanAmount\":600000,\"assetClass\":\"WEL\",\"interestRate\":17,\"loanPSLClassification\":\"SC and ST tribe\",\"gender\":\"Female\",\"caste\":\"ST\",\"subSectorCode\":\"Manufacture of Textiles\",\"applicationDate\":\"2018-11-09\",\"disbursementDate\":\"2018-11-14\",\"dpdAsOnDate\":84,\"cityType\":\"B30\",\"lenderName\":\"B\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":15},{\"bcName\":\"MSM\",\"location\":\"Gujarat\",\"loanAmount\":500000,\"assetClass\":\"KCC loan\",\"interestRate\":19,\"loanPSLClassification\":\"SC and ST tribe\",\"gender\":\"Male\",\"caste\":\"SC\",\"subSectorCode\":\"Agriculture & Related Service Activities\",\"applicationDate\":\"2017-04-01\",\"disbursementDate\":\"2017-04-10\",\"dpdAsOnDate\":34,\"cityType\":\"B30\",\"lenderName\":\"B\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":15},{\"bcName\":\"MSM\",\"location\":\"Madhya Pradesh\",\"loanAmount\":600000,\"assetClass\":\"JLG\",\"interestRate\":18,\"loanPSLClassification\":\"Minority community\",\"gender\":\"Female\",\"caste\":\"ST\",\"subSectorCode\":\"CONSTRUCTION\",\"applicationDate\":\"2018-08-24\",\"disbursementDate\":\"2018-09-03\",\"dpdAsOnDate\":38,\"cityType\":\"B30\",\"lenderName\":\"B\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":15},{\"bcName\":\"MSM\",\"location\":\"Madhya Pradesh\",\"loanAmount\":800000,\"assetClass\":\"IL\",\"interestRate\":24,\"loanPSLClassification\":\"Export credit\",\"gender\":\"Female\",\"caste\":\"General\",\"subSectorCode\":\"Farming of Animals, Poultry\",\"applicationDate\":\"2017-01-30\",\"disbursementDate\":\"2017-02-07\",\"dpdAsOnDate\":0,\"cityType\":\"T30\",\"lenderName\":\"B\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":15},{\"bcName\":\"MSM\",\"location\":\"Madhya Pradesh\",\"loanAmount\":500000,\"assetClass\":\"WEL\",\"interestRate\":25,\"loanPSLClassification\":\"Export credit\",\"gender\":\"Female\",\"caste\":\"General\",\"subSectorCode\":\"Farming of Animals, Poultry\",\"applicationDate\":\"2018-06-12\",\"disbursementDate\":\"2018-06-21\",\"dpdAsOnDate\":28,\"cityType\":\"B30\",\"lenderName\":\"A\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":15},{\"bcName\":\"MSM\",\"location\":\"Madhya Pradesh\",\"loanAmount\":600000,\"assetClass\":\"KCC loan\",\"interestRate\":22,\"loanPSLClassification\":\"Minority community\",\"gender\":\"Male\",\"caste\":\"SC\",\"subSectorCode\":\"Agriculture & Related Service Activities\",\"applicationDate\":\"2017-09-05\",\"disbursementDate\":\"2017-09-13\",\"dpdAsOnDate\":73,\"cityType\":\"B30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":15},{\"bcName\":\"MSM\",\"location\":\"Bihar\",\"loanAmount\":500000,\"assetClass\":\"KCC loan\",\"interestRate\":23,\"loanPSLClassification\":\"Minority community\",\"gender\":\"Other\",\"caste\":\"SC\",\"subSectorCode\":\"Agriculture & Related Service Activities\",\"applicationDate\":\"2018-03-20\",\"disbursementDate\":\"2018-03-27\",\"dpdAsOnDate\":5,\"cityType\":\"B30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":15},{\"bcName\":\"MSM\",\"location\":\"Bihar\",\"loanAmount\":50000,\"assetClass\":\"JLG\",\"interestRate\":16,\"loanPSLClassification\":\"Minority community\",\"gender\":\"Male\",\"caste\":\"ST\",\"subSectorCode\":\"CONSTRUCTION\",\"applicationDate\":\"2017-12-15\",\"disbursementDate\":\"2017-12-25\",\"dpdAsOnDate\":65,\"cityType\":\"B30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":15},{\"bcName\":\"MSM\",\"location\":\"Bihar\",\"loanAmount\":100000,\"assetClass\":\"Two-wheeler loan\",\"interestRate\":15,\"loanPSLClassification\":\"SC and ST tribe\",\"gender\":\"Male\",\"caste\":\"SC\",\"subSectorCode\":\"Farming of Animals, Poultry\",\"applicationDate\":\"2018-10-02\",\"disbursementDate\":\"2018-10-12\",\"dpdAsOnDate\":38,\"cityType\":\"B30\",\"lenderName\":\"A\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":15},{\"bcName\":\"MSM\",\"location\":\"Uttar Pradesh\",\"loanAmount\":800000,\"assetClass\":\"Tractor Loan\",\"interestRate\":26,\"loanPSLClassification\":\"Distressed farmer\",\"gender\":\"Female\",\"caste\":\"ST\",\"subSectorCode\":\"Agriculture & Related Service Activities\",\"applicationDate\":\"2017-05-10\",\"disbursementDate\":\"2017-05-19\",\"dpdAsOnDate\":0,\"cityType\":\"T30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":15},{\"bcName\":\"MSM\",\"location\":\"Uttar Pradesh\",\"loanAmount\":700000,\"assetClass\":\"KCC loan\",\"interestRate\":19,\"loanPSLClassification\":\"Person with disability\",\"gender\":\"Male\",\"caste\":\"General\",\"subSectorCode\":\"Agriculture & Related Service Activities\",\"applicationDate\":\"2018-02-18\",\"disbursementDate\":\"2018-02-26\",\"dpdAsOnDate\":77,\"cityType\":\"T30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":15},{\"bcName\":\"Swasti\",\"location\":\"Tamil Nadu\",\"loanAmount\":500000,\"assetClass\":\"Tractor Loan\",\"interestRate\":20,\"loanPSLClassification\":\"Distressed farmer\",\"gender\":\"Male\",\"caste\":\"SC\",\"subSectorCode\":\"Agriculture & Related Service Activities\",\"applicationDate\":\"2017-07-03\",\"disbursementDate\":\"2017-07-09\",\"dpdAsOnDate\":60,\"cityType\":\"B30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":23},{\"bcName\":\"Swasti\",\"location\":\"Tamil Nadu\",\"loanAmount\":800000,\"assetClass\":\"WEL\",\"interestRate\":22,\"loanPSLClassification\":\"SC and ST tribe\",\"gender\":\"Female\",\"caste\":\"SC\",\"subSectorCode\":\"Farming of Animals, Poultry\",\"applicationDate\":\"2018-11-25\",\"disbursementDate\":\"2018-12-02\",\"dpdAsOnDate\":67,\"cityType\":\"B30\",\"lenderName\":\"B\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":23},{\"bcName\":\"Swasti\",\"location\":\"Rajasthan\",\"loanAmount\":500000,\"assetClass\":\"WEL\",\"interestRate\":21,\"loanPSLClassification\":\"SC and ST tribe\",\"gender\":\"Female\",\"caste\":\"SC\",\"subSectorCode\":\"Manufacture of Textiles\",\"applicationDate\":\"2017-04-08\",\"disbursementDate\":\"2017-04-17\",\"dpdAsOnDate\":0,\"cityType\":\"B30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":23},{\"bcName\":\"Swasti\",\"location\":\"Rajasthan\",\"loanAmount\":700000,\"assetClass\":\"JLG\",\"interestRate\":21,\"loanPSLClassification\":\"SC and ST tribe\",\"gender\":\"Female\",\"caste\":\"ST\",\"subSectorCode\":\"Manufacture of furniture\",\"applicationDate\":\"2018-08-31\",\"disbursementDate\":\"2018-09-07\",\"dpdAsOnDate\":2,\"cityType\":\"B30\",\"lenderName\":\"B\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":23},{\"bcName\":\"Swasti\",\"location\":\"Rajasthan\",\"loanAmount\":800000,\"assetClass\":\"WEL\",\"interestRate\":16,\"loanPSLClassification\":\"SC and ST tribe\",\"gender\":\"Female\",\"caste\":\"ST\",\"subSectorCode\":\"Manufacture of furniture\",\"applicationDate\":\"2017-01-22\",\"disbursementDate\":\"2017-02-01\",\"dpdAsOnDate\":0,\"cityType\":\"T30\",\"lenderName\":\"B\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":23},{\"bcName\":\"Swasti\",\"location\":\"Rajasthan\",\"loanAmount\":700000,\"assetClass\":\"KCC loan\",\"interestRate\":18,\"loanPSLClassification\":\"Export credit\",\"gender\":\"Female\",\"caste\":\"General\",\"subSectorCode\":\"Agriculture & Related Service Activities\",\"applicationDate\":\"2018-06-05\",\"disbursementDate\":\"2018-06-13\",\"dpdAsOnDate\":0,\"cityType\":\"T30\",\"lenderName\":\"A\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":23},{\"bcName\":\"Swasti\",\"location\":\"Assam\",\"loanAmount\":500000,\"assetClass\":\"WEL\",\"interestRate\":24,\"loanPSLClassification\":\"SC and ST tribe\",\"gender\":\"Female\",\"caste\":\"ST\",\"subSectorCode\":\"Manufacture of Textiles\",\"applicationDate\":\"2017-09-26\",\"disbursementDate\":\"2017-10-01\",\"dpdAsOnDate\":5,\"cityType\":\"T30\",\"lenderName\":\"A\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":23},{\"bcName\":\"Swasti\",\"location\":\"Assam\",\"loanAmount\":150000,\"assetClass\":\"Two-wheeler loan\",\"interestRate\":25,\"loanPSLClassification\":\"SC and ST tribe\",\"gender\":\"Male\",\"caste\":\"SC\",\"subSectorCode\":\"Manufacture of furniture\",\"applicationDate\":\"2018-03-14\",\"disbursementDate\":\"2018-03-24\",\"dpdAsOnDate\":0,\"cityType\":\"T30\",\"lenderName\":\"A\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":23},{\"bcName\":\"Swasti\",\"location\":\"Jharkhand\",\"loanAmount\":600000,\"assetClass\":\"Tractor Loan\",\"interestRate\":17,\"loanPSLClassification\":\"Export credit\",\"gender\":\"Female\",\"caste\":\"General\",\"subSectorCode\":\"Agriculture & Related Service Activities\",\"applicationDate\":\"2017-12-10\",\"disbursementDate\":\"2017-12-19\",\"dpdAsOnDate\":0,\"cityType\":\"T30\",\"lenderName\":\"B\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":23},{\"bcName\":\"Swasti\",\"location\":\"Jharkhand\",\"loanAmount\":60000,\"assetClass\":\"IL\",\"interestRate\":18,\"loanPSLClassification\":\"Export credit\",\"gender\":\"Female\",\"caste\":\"General\",\"subSectorCode\":\"Farming of Animals, Poultry\",\"applicationDate\":\"2018-10-17\",\"disbursementDate\":\"2018-10-23\",\"dpdAsOnDate\":0,\"cityType\":\"B30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":23},{\"bcName\":\"Subahlaxmi\",\"location\":\"Tamil Nadu\",\"loanAmount\":80000,\"assetClass\":\"IL\",\"interestRate\":15,\"loanPSLClassification\":\"Person with disability\",\"gender\":\"Female\",\"caste\":\"OBC\",\"subSectorCode\":\"Manufacture of furniture\",\"applicationDate\":\"2017-05-27\",\"disbursementDate\":\"2017-06-05\",\"dpdAsOnDate\":0,\"cityType\":\"T30\",\"lenderName\":\"A\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":26},{\"bcName\":\"Subahlaxmi\",\"location\":\"Tamil Nadu\",\"loanAmount\":100000,\"assetClass\":\"Two-wheeler loan\",\"interestRate\":15,\"loanPSLClassification\":\"Small Artisians\",\"gender\":\"Male\",\"caste\":\"General\",\"subSectorCode\":\"Manufacture of Textiles\",\"applicationDate\":\"2018-02-22\",\"disbursementDate\":\"2018-03-02\",\"dpdAsOnDate\":0,\"cityType\":\"B30\",\"lenderName\":\"B\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":26},{\"bcName\":\"Subahlaxmi\",\"location\":\"Tamil Nadu\",\"loanAmount\":50000,\"assetClass\":\"WEL\",\"interestRate\":23,\"loanPSLClassification\":\"NRLM Benificiary\",\"gender\":\"Female\",\"caste\":\"General\",\"subSectorCode\":\"Manufacture of Beverages\",\"applicationDate\":\"2017-07-13\",\"disbursementDate\":\"2017-07-21\",\"dpdAsOnDate\":0,\"cityType\":\"B30\",\"lenderName\":\"A\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":26},{\"bcName\":\"Subahlaxmi\",\"location\":\"Tamil Nadu\",\"loanAmount\":100000,\"assetClass\":\"Two-wheeler loan\",\"interestRate\":14,\"loanPSLClassification\":\"Distressed farmer\",\"gender\":\"Male\",\"caste\":\"ST\",\"subSectorCode\":\"Manufacture of footwear\",\"applicationDate\":\"2018-11-04\",\"disbursementDate\":\"2018-11-12\",\"dpdAsOnDate\":83,\"cityType\":\"B30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":26},{\"bcName\":\"Subahlaxmi\",\"location\":\"Chattisgarh\",\"loanAmount\":50000,\"assetClass\":\"IL\",\"interestRate\":23,\"loanPSLClassification\":\"Person with disability\",\"gender\":\"Male\",\"caste\":\"SC\",\"subSectorCode\":\"Manufacture of Beverages\",\"applicationDate\":\"2017-04-24\",\"disbursementDate\":\"2017-05-01\",\"dpdAsOnDate\":0,\"cityType\":\"T30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":26},{\"bcName\":\"Subahlaxmi\",\"location\":\"Chattisgarh\",\"loanAmount\":50000,\"assetClass\":\"JLG\",\"interestRate\":20,\"loanPSLClassification\":\"Distressed farmer\",\"gender\":\"Female\",\"caste\":\"OBC\",\"subSectorCode\":\"Manufacture of Beverages\",\"applicationDate\":\"2018-08-17\",\"disbursementDate\":\"2018-08-25\",\"dpdAsOnDate\":1,\"cityType\":\"T30\",\"lenderName\":\"A\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":26},{\"bcName\":\"Subahlaxmi\",\"location\":\"Chattisgarh\",\"loanAmount\":50000,\"assetClass\":\"IL\",\"interestRate\":19,\"loanPSLClassification\":\"Export credit\",\"gender\":\"Female\",\"caste\":\"General\",\"subSectorCode\":\"Manufacture of furniture\",\"applicationDate\":\"2017-01-08\",\"disbursementDate\":\"2017-01-17\",\"dpdAsOnDate\":40,\"cityType\":\"B30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":26},{\"bcName\":\"Subahlaxmi\",\"location\":\"Chattisgarh\",\"loanAmount\":700000,\"assetClass\":\"Tractor Loan\",\"interestRate\":24,\"loanPSLClassification\":\"Distressed farmer\",\"gender\":\"Other\",\"caste\":\"OBC\",\"subSectorCode\":\"Agriculture & Related Service Activities\",\"applicationDate\":\"2018-06-21\",\"disbursementDate\":\"2018-06-29\",\"dpdAsOnDate\":63,\"cityType\":\"T30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":26},{\"bcName\":\"Subahlaxmi\",\"location\":\"Chattisgarh\",\"loanAmount\":600000,\"assetClass\":\"Tractor Loan\",\"interestRate\":18,\"loanPSLClassification\":\"Export credit\",\"gender\":\"Female\",\"caste\":\"General\",\"subSectorCode\":\"Agriculture & Related Service Activities\",\"applicationDate\":\"2017-09-12\",\"disbursementDate\":\"2017-09-20\",\"dpdAsOnDate\":0,\"cityType\":\"B30\",\"lenderName\":\"A\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":26},{\"bcName\":\"Subahlaxmi\",\"location\":\"Madhya Pradesh\",\"loanAmount\":50000,\"assetClass\":\"WEL\",\"interestRate\":22,\"loanPSLClassification\":\"Person with disability\",\"gender\":\"Female\",\"caste\":\"OBC\",\"subSectorCode\":\"Manufacture of Textiles\",\"applicationDate\":\"2018-03-02\",\"disbursementDate\":\"2018-03-10\",\"dpdAsOnDate\":0,\"cityType\":\"T30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":26},{\"bcName\":\"Subahlaxmi\",\"location\":\"Madhya Pradesh\",\"loanAmount\":700000,\"assetClass\":\"KCC loan\",\"interestRate\":22,\"loanPSLClassification\":\"SC and ST tribe\",\"gender\":\"Female\",\"caste\":\"SC\",\"subSectorCode\":\"Agriculture & Related Service Activities\",\"applicationDate\":\"2017-12-19\",\"disbursementDate\":\"2017-12-26\",\"dpdAsOnDate\":0,\"cityType\":\"T30\",\"lenderName\":\"B\",\"fLDGOffered\":\"No\",\"yearsOfOperation\":26},{\"bcName\":\"Subahlaxmi\",\"location\":\"Andhra Pradesh\",\"loanAmount\":700000,\"assetClass\":\"Tractor Loan\",\"interestRate\":22,\"loanPSLClassification\":\"Small and Marginal farmer\",\"gender\":\"Male\",\"caste\":\"ST\",\"subSectorCode\":\"Agriculture & Related Service Activities\",\"applicationDate\":\"2018-10-08\",\"disbursementDate\":\"2018-10-15\",\"dpdAsOnDate\":43,\"cityType\":\"T30\",\"lenderName\":\"B\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":26},{\"bcName\":\"Subahlaxmi\",\"location\":\"Andhra Pradesh\",\"loanAmount\":800000,\"assetClass\":\"KCC loan\",\"interestRate\":20,\"loanPSLClassification\":\"Minority community\",\"gender\":\"Female\",\"caste\":\"ST\",\"subSectorCode\":\"Agriculture & Related Service Activities\",\"applicationDate\":\"2017-05-16\",\"disbursementDate\":\"2017-05-22\",\"dpdAsOnDate\":56,\"cityType\":\"T30\",\"lenderName\":\"B\",\"fLDGOffered\":\"Yes\",\"yearsOfOperation\":26}]";
        return objectMapper.readValue(ins, new TypeReference<List<LenderDto>>() {
        });
    }

    @GetMapping("/DA")
    public ResponseEntity<List<DAResponse>> getDAData(@RequestParam(required = false) String totalOS,
                                                      @RequestParam(required = false) String maturingYear,
                                                      @RequestParam(required = false) String loanCycle,
                                                      @RequestParam(required = false) String subSector,
                                                      @RequestParam(required = false) String cibilScore,
                                                      @RequestParam(required = false) String euitableOrRegisteredMortage,
                                                      @RequestParam(required = false) String seaoning,
                                                      @RequestParam(required = false) String tenure,
                                                      @RequestParam(required = false) String sortBy
    ) throws IOException {
        final List<DAResponse>[] daApiResponse = new List[]{new ArrayList<>()};
        List<DADto> daDtoList = getDaDtos();
        List<String> orderList = daDtoList.stream().map(DADto::getPoolName).distinct().collect(Collectors.toList());
        List<DADto> finalLenderDtoList = daDtoList;
        List<DADto> finalLenderDtoList1 = daDtoList;

        orderList.forEach(s -> {
            DAResponse daResponse = new DAResponse();
            daResponse.setPoolName(s);
            List<DADto> daDtos = finalLenderDtoList1.stream().filter(lenderDto -> StringUtils.equalsIgnoreCase(lenderDto.getPoolName(), s)).collect(Collectors.toList());
            List<DADto> daDtos1 = daDtos;
            daResponse.setPoolAvailability(daDtos.get(0).getPoolAvailability());
            daResponse.setKiRating(Double.valueOf(daDtos.get(0).getKiRating()));
            if (StringUtils.isNotBlank(totalOS)) {
                if("lessThank10Lakh".equalsIgnoreCase(totalOS)) {
                    daDtos1 = daDtos1.stream().filter(dto -> {
                        Double os = Double.valueOf(dto.getTotalOS().replaceAll(",",""));
                        return os<1000000D;
                    }).collect(Collectors.toList());
                } else if ("greaterThanOrEqualsTo10LakhAndlessThan20Lakh".equalsIgnoreCase(totalOS)) {
                    daDtos1 = daDtos1.stream().filter(dto -> {
                        Double os = Double.valueOf(dto.getTotalOS().replaceAll(",",""));
                        return os>=1000000D && os<=2000000;
                    }).collect(Collectors.toList());
                } else if ("GreaterThanOrEqualsTo20Lakh".equalsIgnoreCase(totalOS)) {
                    daDtos1 = daDtos1.stream().filter(dto -> {
                        Double os = Double.valueOf(dto.getTotalOS().replaceAll(",",""));
                        return os>=2000000;
                    }).collect(Collectors.toList());
                }
            }
            if (StringUtils.isNotBlank(maturingYear)) {
                daDtos1 = daDtos1.stream().filter(dto -> StringUtils.equalsIgnoreCase(dto.getMaturingYear(), maturingYear)).collect(Collectors.toList());
            }
            if (StringUtils.isNotBlank(loanCycle)) {
                daDtos1 = daDtos1.stream().filter(dto -> StringUtils.equalsIgnoreCase(dto.getLoanCycle(), loanCycle)).collect(Collectors.toList());
            }
            if (StringUtils.isNotBlank(subSector)) {
                daDtos1 = daDtos1.stream().filter(dto -> StringUtils.equalsIgnoreCase(dto.getSubSector(), subSector)).collect(Collectors.toList());
            }
            if (StringUtils.isNotBlank(cibilScore)) {
                if ("lessThan700".equalsIgnoreCase(cibilScore)) {
                    daDtos1 = daDtos1.stream().filter(dto -> {
                        Double os = Double.valueOf(dto.getCibilScore());
                        return os < 700D;
                    }).collect(Collectors.toList());
                } else if ("greaterThanOrEquals700AndLessThan750".equalsIgnoreCase(cibilScore)) {
                    daDtos1 = daDtos1.stream().filter(dto -> {
                        Double os = Double.valueOf(dto.getCibilScore());
                        return os >= 700D && os < 750D;
                    }).collect(Collectors.toList());
                } else if ("greaterThanOrEquals750".equalsIgnoreCase(cibilScore)) {
                    daDtos1 = daDtos1.stream().filter(dto -> {
                        Double os = Double.valueOf(dto.getCibilScore());
                        return os >= 750D;
                    }).collect(Collectors.toList());
                }
            }
            if (StringUtils.isNotBlank(euitableOrRegisteredMortage)) {
                daDtos1 = daDtos1.stream().filter(lenderDto -> StringUtils.equalsIgnoreCase(lenderDto.getMortagage(), euitableOrRegisteredMortage)).collect(Collectors.toList());
            }
            if (StringUtils.isNotBlank(seaoning)) {
                if ("lessThan15".equalsIgnoreCase(seaoning)) {
                    daDtos1 = daDtos1.stream().filter(dto -> {
                        Double os = Double.valueOf(dto.getSeaoning());
                        return os < 15D;
                    }).collect(Collectors.toList());
                } else if ("greaterThanOrEqualsTo15AndLessThan25".equalsIgnoreCase(seaoning)) {
                    daDtos1 = daDtos1.stream().filter(dto -> {
                        Double os = Double.valueOf(dto.getSeaoning());
                        return os >= 15D && os < 25D;
                    }).collect(Collectors.toList());
                } else if ("greaterThanOrEqualsTo25".equalsIgnoreCase(seaoning)) {
                    daDtos1 = daDtos1.stream().filter(dto -> {
                        Double os = Double.valueOf(dto.getSeaoning());
                        return os >= 25D;
                    }).collect(Collectors.toList());
                }
            }
            if (StringUtils.isNotBlank(tenure)) {
                if("lessThanOrEquslsTo85".equalsIgnoreCase(tenure)) {
                    daDtos1 = daDtos1.stream().filter(dto -> {
                        Double os = Double.valueOf(dto.getTenure());
                        return os<=85;
                    }).collect(Collectors.toList());
                } else if ("greaterThan85".equalsIgnoreCase(tenure)) {
                    daDtos1 = daDtos1.stream().filter(dto -> {
                        Double os = Double.valueOf(dto.getTenure());
                        return os>85;
                    }).collect(Collectors.toList());
                }
            }
            daResponse.setPoolName(s);
            daResponse.setVolume(daDtos1.stream().mapToLong(daDto -> {
                String abc = daDto.getTotalOS().replaceAll(",", "").substring(0, daDto.getTotalOS().lastIndexOf('.') - 2).trim();
                if (StringUtils.isNotBlank(abc)) {
                    return Long.parseLong(abc);
                } else {
                    return 0L;
                }
            }).sum());
            daDtos1.stream().mapToDouble(daDto -> Double.parseDouble(daDto.getSeaoning())).average().ifPresent(seasoning -> daResponse.setAverageSeasoning(Double.valueOf(String.format("%.2f", seasoning))));
            double filteredSize = daDtos1.size();
            double realSize = daDtos.size();
            daResponse.setSearchMatchPercentage(Integer.valueOf((int) ((filteredSize / realSize) * 100D)));
            if (daDtos1.size() >= 1) {
                daApiResponse[0].add(daResponse);
            }
            if (StringUtils.isNotBlank(sortBy)) {
                daApiResponse[0] = daApiResponse[0].stream().sorted(Comparator.comparingDouble(p -> -p.getKiRating())).collect(Collectors.toList());
            }
        });
        return ResponseEntity.ok().body(daApiResponse[0]);
    }

    private List<DADto> getDaDtos() throws IOException {
        FileInputStream fis = new FileInputStream("DaPools.txt");

        String stringTooLong = IOUtils.toString(fis, Charset.defaultCharset());
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(stringTooLong, new TypeReference<List<DADto>>() {
        });
    }
}