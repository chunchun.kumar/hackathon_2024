FROM openjdk:8-jdk-alpine
RUN apk add -U tzdata
RUN ln -fs /usr/share/zoneinfo/Asia/Kolkata /etc/localtime
EXPOSE 8091
RUN mkdir -p /vol/LOGS/DASHBOARD_SERVICE
RUN mkdir -p /app
COPY build/libs/*.war  /app/dashboardservice.war
ENTRYPOINT ["java","-jar","-Dspring.application.hosturl=dev","/app/dashboardservice.war"]
